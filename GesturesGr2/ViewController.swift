//
//  ViewController.swift
//  GesturesGr2
//
//  Created by alexfb on 30/6/17.
//  Copyright © 2017 EPN. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var funcionLabel: UILabel!
    @IBOutlet weak var touchesLabel: UILabel!
    @IBOutlet weak var tapLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var gestureView: UIView!
    @IBOutlet var tapGesture: UITapGestureRecognizer!

    @IBOutlet weak var wConstraint: NSLayoutConstraint!
    @IBOutlet weak var hConstraint: NSLayoutConstraint!

    var orgH, orgW, orgX, orgY:CGFloat!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        tapGesture.numberOfTapsRequired=5
        tapGesture.numberOfTouchesRequired=2
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        orgH=hConstraint.constant
        orgW=wConstraint.constant
               orgX=hConstraint.constant
               orgY=wConstraint.constant
        
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        funcionLabel.text="TouchesBegan"
        touchesLabel.text="\(touches.count)"
    let touch=touches.first!
        tapLabel.text="\(touch.tapCount ?? 0)"
       /* switch touch.tapCount{
        case 1...5:
            gestureView.backgroundColor=UIColor.black
        case 6...10:
            gestureView.backgroundColor=UIColor.cyan
        case 11...15:
            gestureView.backgroundColor=UIColor.brown
        default:
            gestureView.backgroundColor=UIColor.darkGray
        }*/
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        funcionLabel.text="TochesMoved"
        let touch=touches.first!
        locationLabel.text="\(touch.location(in:view))"
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?){
        funcionLabel.text="TouchesEnded"
        locationLabel.text="(0,0)"
        touchesLabel.text="0"
        tapLabel.text="0"
    }
    

    
    
    @IBAction func tapGestureOnView(_ sender: Any) {
     let gesture=sender as! UITapGestureRecognizer
        gestureView.backgroundColor=UIColor.red
    }
   

    @IBAction func pinchOnView(_ sender: UIPinchGestureRecognizer) {
        
        hConstraint.constant=orgH*sender.scale
        wConstraint.constant=orgW*sender.scale
        print(sender.velocity)
        print(sender.scale)
        
        if sender.velocity>12{
            hConstraint.constant=view.frame.height
            wConstraint.constant=view.frame.width
            sender.isEnabled=false
        }
        
        if sender.state == .ended{
            orgH=hConstraint.constant
            orgW=wConstraint.constant
        }
        
        if sender.state == .cancelled{
            orgH=hConstraint.constant
            orgW=wConstraint.constant
            sender.isEnabled=true
        }
    }
    @IBAction func panOnView(_ sender: UIPanGestureRecognizer) {
        print(sender.translation(in: view))
        
 
        gestureView.center.x=orgX+sender.translation(in: view).x
        gestureView.center.y=orgY+sender.translation(in: view).y
            //CGPoint(x: sender.translation(in: view), y: sender.translation(in: view))
        if sender.state == .ended{
            orgX=gestureView.center.x
            orgY=gestureView.center.y
        }
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

